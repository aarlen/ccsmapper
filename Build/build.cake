#tool nuget:?package=Microsoft.TestPlatform
#tool nuget:?package=vswhere
#addin "Cake.Npm"
#addin "Cake.Nuget"
#addin "Cake.Figlet"

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");
var currentDir = System.IO.Directory.GetCurrentDirectory();
var solutionFile = System.IO.Path.Combine(currentDir,"..","Solutions","CCSMapper.sln");
var outputDir = System.IO.Path.Combine(currentDir,"..","Run");

Task("Restore-Packages")
    .Does(() =>
{
    Information("Restoring packages of {0}",solutionFile);
    var settings = new NuGetRestoreSettings() {
        Verbosity = NuGetVerbosity.Detailed
    };
    NuGetRestore(solutionFile,settings);
});

Task("Build")
    .IsDependentOn("Restore-Packages")
    .Does(() =>
{   
    Information("Building {0} into {1}",solutionFile,outputDir);

    DirectoryPath vsLatest  = VSWhereLatest();
    FilePath msBuildPath = vsLatest.CombineWithFilePath("./MSBuild/15.0/Bin/MSBuild.exe");

    Information("msbuild path {0}",msBuildPath);

    var settings = new MSBuildSettings() {
        ToolPath = msBuildPath
    };
    
    settings.SetConfiguration("Release")
            .UseToolVersion(MSBuildToolVersion.Default)
            .WithTarget("Build")
            .SetVerbosity(Verbosity.Normal)
            .WithProperty("OutDir", outputDir);

    MSBuild(solutionFile, settings);
});

Task("Run-Unit-Tests")
    .IsDependentOn("Build")
    .Does(() =>
{
    var testAssemblies = outputDir + "\\*UnitTests.dll";
    VSTest(testAssemblies);
});

Task("Default")
    .IsDependentOn("Run-Unit-Tests");

Information(Figlet("CCSMapper"));
RunTarget(target);
