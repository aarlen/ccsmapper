﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.ImageRecognition
{
    public class ImageRecognitionOptions
    {
        public double CannyThreshold { get; set; }
        public double CircleAccumulatorThreshold { get; set; }
        public double InverseAccumulatorRatio { get; set; }
        public double MinDistBetweenCircles { get; set; }
        public int MinRadius { get; set; }
        public int MaxRadius { get; set; }

        public ImageRecognitionOptions()
        {
            CannyThreshold = 100.0;
            CircleAccumulatorThreshold = 100;
            InverseAccumulatorRatio = 2.0;
            MinDistBetweenCircles = 20.0;
            MinRadius = 5;
            MaxRadius = 0;
        }
    }
}
