﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using CCSMapper.API.TextRecognition.DTO;
using CCSMapper.API.UconRecognition;

namespace API.ImageRecognition
{
    public class RecognisedImageDetail
    {
        public Image Image { get; set; }
        public Image CircleMask { get; set; }
        public IEnumerable<RecognisedCharacterDetail> RecognisedCharacters { get; set; }
        public IEnumerable<UconShapeDetail> RecognisedShapes { get; set; }
    }
}
