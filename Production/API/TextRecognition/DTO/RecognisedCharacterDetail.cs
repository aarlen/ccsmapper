﻿using System.Drawing;

namespace CCSMapper.API.TextRecognition.DTO
{
    public class RecognisedCharacterDetail
    {
        public string Text { get; set; }
        public Rectangle Region { get; set; }
        public float Cost { get; set; }
    }
}
