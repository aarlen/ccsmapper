﻿using System.Drawing;

namespace CCSMapper.API.TextRecognition.DTO
{
    public class RecognisedWordDetail
    {
        public string Text { get; set; }
        public Rectangle Region { get; set; }
    }
}
