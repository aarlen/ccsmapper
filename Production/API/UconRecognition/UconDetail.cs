﻿using System.Collections.Generic;
using System.Drawing;

namespace CCSMapper.API.UconRecognition
{
    public class UconDetail
    {
        public IEnumerable<string> Text { get; set; }
        public Rectangle Region { get; set; }
    }
}
