﻿using System.Drawing;

namespace CCSMapper.API.UconRecognition
{
    public class UconShapeDetail
    {
        public PointF Center { get; set; }

        public float Radius { get; set; }

        public double Area { get; set; }
    }
}
