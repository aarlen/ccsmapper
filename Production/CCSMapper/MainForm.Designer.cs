﻿namespace CCSMapper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.runBtn = new System.Windows.Forms.Button();
            this.originalImageBox = new System.Windows.Forms.PictureBox();
            this.circleImageBox = new System.Windows.Forms.PictureBox();
            this.msgList = new System.Windows.Forms.ListBox();
            this.browseFileBtn = new System.Windows.Forms.Button();
            this.maxRadiusTrack = new CCSMapper.TrackWithLabel();
            this.minRadiusTrack = new CCSMapper.TrackWithLabel();
            this.minDistanceBetweenCirclesTrack = new CCSMapper.TrackWithLabel();
            this.inverseAccumulatorTrack = new CCSMapper.TrackWithLabel();
            this.circleAccumulatorTrack = new CCSMapper.TrackWithLabel();
            this.cannyThresholdTrack = new CCSMapper.TrackWithLabel();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.originalImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circleImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // runBtn
            // 
            this.runBtn.Location = new System.Drawing.Point(12, 40);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(794, 26);
            this.runBtn.TabIndex = 1;
            this.runBtn.Text = "Process";
            this.runBtn.UseVisualStyleBackColor = true;
            this.runBtn.Click += new System.EventHandler(this.OnRunClicked);
            // 
            // originalImageBox
            // 
            this.originalImageBox.Location = new System.Drawing.Point(12, 70);
            this.originalImageBox.Name = "originalImageBox";
            this.originalImageBox.Size = new System.Drawing.Size(794, 400);
            this.originalImageBox.TabIndex = 2;
            this.originalImageBox.TabStop = false;
            // 
            // circleImageBox
            // 
            this.circleImageBox.Location = new System.Drawing.Point(12, 478);
            this.circleImageBox.Name = "circleImageBox";
            this.circleImageBox.Size = new System.Drawing.Size(794, 400);
            this.circleImageBox.TabIndex = 4;
            this.circleImageBox.TabStop = false;
            // 
            // msgList
            // 
            this.msgList.FormattingEnabled = true;
            this.msgList.ItemHeight = 16;
            this.msgList.Location = new System.Drawing.Point(819, 655);
            this.msgList.Name = "msgList";
            this.msgList.Size = new System.Drawing.Size(623, 228);
            this.msgList.TabIndex = 5;
            // 
            // browseFileBtn
            // 
            this.browseFileBtn.Location = new System.Drawing.Point(755, 12);
            this.browseFileBtn.Name = "browseFileBtn";
            this.browseFileBtn.Size = new System.Drawing.Size(51, 23);
            this.browseFileBtn.TabIndex = 18;
            this.browseFileBtn.Text = "...";
            this.browseFileBtn.UseVisualStyleBackColor = true;
            this.browseFileBtn.Click += new System.EventHandler(this.OnFileBrowseClicked);
            // 
            // maxRadiusTrack
            // 
            this.maxRadiusTrack.Label = "Max Radius";
            this.maxRadiusTrack.Location = new System.Drawing.Point(962, 445);
            this.maxRadiusTrack.Maximum = 10;
            this.maxRadiusTrack.Minimum = 0;
            this.maxRadiusTrack.Name = "maxRadiusTrack";
            this.maxRadiusTrack.Size = new System.Drawing.Size(500, 100);
            this.maxRadiusTrack.TabIndex = 17;
            this.maxRadiusTrack.Value = 0;
            // 
            // minRadiusTrack
            // 
            this.minRadiusTrack.Label = "Min Radius";
            this.minRadiusTrack.Location = new System.Drawing.Point(962, 545);
            this.minRadiusTrack.Maximum = 10;
            this.minRadiusTrack.Minimum = 0;
            this.minRadiusTrack.Name = "minRadiusTrack";
            this.minRadiusTrack.Size = new System.Drawing.Size(500, 100);
            this.minRadiusTrack.TabIndex = 16;
            this.minRadiusTrack.Value = 5;
            // 
            // minDistanceBetweenCirclesTrack
            // 
            this.minDistanceBetweenCirclesTrack.Label = "Min Distance Between Circles";
            this.minDistanceBetweenCirclesTrack.Location = new System.Drawing.Point(962, 345);
            this.minDistanceBetweenCirclesTrack.Maximum = 100;
            this.minDistanceBetweenCirclesTrack.Minimum = 1;
            this.minDistanceBetweenCirclesTrack.Name = "minDistanceBetweenCirclesTrack";
            this.minDistanceBetweenCirclesTrack.Size = new System.Drawing.Size(500, 100);
            this.minDistanceBetweenCirclesTrack.TabIndex = 15;
            this.minDistanceBetweenCirclesTrack.Value = 20;
            // 
            // inverseAccumulatorTrack
            // 
            this.inverseAccumulatorTrack.Label = "Inverse Accumulator Ratio";
            this.inverseAccumulatorTrack.Location = new System.Drawing.Point(962, 245);
            this.inverseAccumulatorTrack.Maximum = 5;
            this.inverseAccumulatorTrack.Minimum = 1;
            this.inverseAccumulatorTrack.Name = "inverseAccumulatorTrack";
            this.inverseAccumulatorTrack.Size = new System.Drawing.Size(500, 100);
            this.inverseAccumulatorTrack.TabIndex = 14;
            this.inverseAccumulatorTrack.Value = 2;
            // 
            // circleAccumulatorTrack
            // 
            this.circleAccumulatorTrack.Label = "Circle Accumulator";
            this.circleAccumulatorTrack.Location = new System.Drawing.Point(962, 145);
            this.circleAccumulatorTrack.Maximum = 500;
            this.circleAccumulatorTrack.Minimum = 1;
            this.circleAccumulatorTrack.Name = "circleAccumulatorTrack";
            this.circleAccumulatorTrack.Size = new System.Drawing.Size(501, 100);
            this.circleAccumulatorTrack.TabIndex = 13;
            this.circleAccumulatorTrack.Value = 100;
            // 
            // cannyThresholdTrack
            // 
            this.cannyThresholdTrack.Label = "Canny Threshold";
            this.cannyThresholdTrack.Location = new System.Drawing.Point(962, 45);
            this.cannyThresholdTrack.Maximum = 500;
            this.cannyThresholdTrack.Minimum = 1;
            this.cannyThresholdTrack.Name = "cannyThresholdTrack";
            this.cannyThresholdTrack.Size = new System.Drawing.Size(501, 100);
            this.cannyThresholdTrack.TabIndex = 12;
            this.cannyThresholdTrack.Value = 100;
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::CCSMapper.Properties.Settings.Default, "LastFilePath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.fileNameTextBox.Location = new System.Drawing.Point(12, 12);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(737, 22);
            this.fileNameTextBox.TabIndex = 0;
            this.fileNameTextBox.Text = global::CCSMapper.Properties.Settings.Default.LastFilePath;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Image files|*.png|All files|*.*";
            this.openFileDialog1.InitialDirectory = global::CCSMapper.Properties.Settings.Default.LastDirectory;
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.OnFileBrowseOk);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1454, 903);
            this.Controls.Add(this.browseFileBtn);
            this.Controls.Add(this.maxRadiusTrack);
            this.Controls.Add(this.minRadiusTrack);
            this.Controls.Add(this.minDistanceBetweenCirclesTrack);
            this.Controls.Add(this.inverseAccumulatorTrack);
            this.Controls.Add(this.circleAccumulatorTrack);
            this.Controls.Add(this.cannyThresholdTrack);
            this.Controls.Add(this.msgList);
            this.Controls.Add(this.circleImageBox);
            this.Controls.Add(this.originalImageBox);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.fileNameTextBox);
            this.Name = "MainForm";
            this.Text = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.originalImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circleImageBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.Button runBtn;
        private System.Windows.Forms.PictureBox originalImageBox;
        private System.Windows.Forms.PictureBox circleImageBox;
        private System.Windows.Forms.ListBox msgList;
        private TrackWithLabel cannyThresholdTrack;
        private TrackWithLabel circleAccumulatorTrack;
        private TrackWithLabel inverseAccumulatorTrack;
        private TrackWithLabel minDistanceBetweenCirclesTrack;
        private TrackWithLabel minRadiusTrack;
        private TrackWithLabel maxRadiusTrack;
        private System.Windows.Forms.Button browseFileBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}