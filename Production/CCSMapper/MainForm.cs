﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.OCR;
using System.Diagnostics;
using System.IO;
using CCSMapper.API.TextRecognition.DTO;
using AutoMapper;
using CCSMapper.UconRecognition;
using CCSMapper.API.UconRecognition;
using ImageRecognition;
using API.ImageRecognition;

namespace CCSMapper
{
    public partial class MainForm : Form
    {
        private TextRecognition.TextRecognition _textRecognition = new TextRecognition.TextRecognition(20);
        private UconRecognizer _uconRecognizer = new UconRecognizer();
        private ImageRecogniser _imageRecogniser;

        public MainForm()
        {
            InitializeComponent();

        }

        private void OnRunClicked(object sender, EventArgs e)
        {
            msgList.Items.Clear();
            var options = new ImageRecognitionOptions();
            options.CannyThreshold = cannyThresholdTrack.Value;
            options.CircleAccumulatorThreshold = circleAccumulatorTrack.Value;
            options.InverseAccumulatorRatio = inverseAccumulatorTrack.Value;
            options.MaxRadius = maxRadiusTrack.Value;
            options.MinRadius = minRadiusTrack.Value;
            options.MinDistBetweenCircles = minDistanceBetweenCirclesTrack.Value;
            
            _imageRecogniser = new ImageRecogniser(options);
            var results = _imageRecogniser.Recognise(fileNameTextBox.Text);
            originalImageBox.Image = results.Image;
            circleImageBox.Image = results.CircleMask;


            foreach (var foundChar in results.RecognisedCharacters)
            {
                msgList.Items.Add($"found characters {foundChar.Text} at {foundChar.Region.ToString()}");
            }

            var recognisedWords = _textRecognition.ComputeStrings(results.RecognisedCharacters.ToList());
            foreach (var foundWord in recognisedWords)
            {
                msgList.Items.Add($"found string {foundWord.Text} at {foundWord.Region.ToString()}");
            }

            var recognisedUcons = _uconRecognizer.Search(results.RecognisedShapes.ToList(), recognisedWords);

            foreach (var ucons in recognisedUcons)
            {
                var text = string.Join("", ucons.Text);
                msgList.Items.Add($"found ucons {text} at {ucons.Region.ToString()}");
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
            base.OnClosing(e);
        }

        private void OnFileBrowseClicked(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void OnFileBrowseOk(object sender, CancelEventArgs e)
        {
            Properties.Settings.Default.LastFilePath = openFileDialog1.FileName;
            Properties.Settings.Default.LastDirectory = Path.GetDirectoryName(Properties.Settings.Default.LastFilePath);
        }
    }
}
