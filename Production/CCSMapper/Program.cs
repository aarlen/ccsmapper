﻿// ------------------------------------------------------------ 
// Blog post code sample! Read the post on http://en.morzel.net
// ------------------------------------------------------------

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoMapper;
using CCSMapper.API.TextRecognition.DTO;
using static Emgu.CV.OCR.Tesseract;
using CCSMapper.API.UconRecognition;

namespace CCSMapper
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<RecognisedCharacterDetail, Character>();
                cfg.CreateMap<UconShapeDetail, CircleF>();
            });

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
