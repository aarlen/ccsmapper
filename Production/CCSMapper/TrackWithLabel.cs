﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCSMapper
{
    public partial class TrackWithLabel : UserControl
    {
        public string Label
        {
            get
            {
                return label1.Text;
            }

            set
            {
                label1.Text = value;
            }
        }

        public int Maximum
        {
            get
            {
                return trackBar1.Maximum;
            }

            set
            {
                trackBar1.Maximum = value;
            }
        }

        public int Value
        {
            get
            {
                return trackBar1.Value;
            }

            set
            {
                trackBar1.Value = value;
            }
        }


        public int Minimum
        {
            get
            {
                return trackBar1.Minimum;
            }

            set
            {
                trackBar1.Minimum = value;
            }
        }
        public TrackWithLabel()
        {
            InitializeComponent();
            InitializeUpDown();
            
        }

        private void InitializeUpDown()
        {
            numericUpDown1.Maximum = trackBar1.Maximum;
            numericUpDown1.Minimum = trackBar1.Minimum;
            numericUpDown1.Value = trackBar1.Value;
        }

        private void OnTrackChanged(object sender, EventArgs e)
        {
            numericUpDown1.Value = trackBar1.Value;
        }
    }
}
