﻿using AutoMapper;
using CCSMapper.API.TextRecognition.DTO;
using CCSMapper.API.UconRecognition;
using CCSMapper.TextRecognition;
using CCSMapper.UconRecognition;
using Emgu.CV.Structure;
using ImageRecognition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Emgu.CV.OCR.Tesseract;

namespace CCSMapperCmdLine
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 1)
            {
                Console.WriteLine("Usage : CCSMapperCmdLine <imageFilePath>");
                return;
            }

            Mapper.Initialize(cfg => {
                cfg.CreateMap<RecognisedCharacterDetail, Character>();
                cfg.CreateMap<UconShapeDetail, CircleF>();
            });

            string imageFilePath = args[0];
            TextRecognition _textRecognition = new TextRecognition(20);
            UconRecognizer _uconRecognizer = new UconRecognizer();
            ImageRecogniser _imageRecogniser = new ImageRecogniser(new API.ImageRecognition.ImageRecognitionOptions());

            Console.WriteLine($"Recognising {imageFilePath}");
            var results = _imageRecogniser.Recognise(imageFilePath);

            foreach (var foundShape in results.RecognisedShapes)
            {
                Console.WriteLine($"found shapes {foundShape.Center.ToString()} at {foundShape.Radius}");
            }

            foreach (var foundChar in results.RecognisedCharacters)
            {
                Console.WriteLine($"found characters {foundChar.Text} at {foundChar.Region.ToString()}");
            }

            var recognisedWords = _textRecognition.ComputeStrings(results.RecognisedCharacters.ToList());
            foreach (var foundWord in recognisedWords)
            {
                Console.WriteLine($"found string {foundWord.Text} at {foundWord.Region.ToString()}");
            }

            var recognisedUcons = _uconRecognizer.Search(results.RecognisedShapes.ToList(), recognisedWords);

            foreach (var ucons in recognisedUcons)
            {
                var text = string.Join("", ucons.Text);
                Console.WriteLine($"found ucons {text} at {ucons.Region.ToString()}");
            }

            Console.WriteLine("complete");
            throw new ApplicationException("Crashing to avoid hang. Try to fix in future");
        }
    }
}
