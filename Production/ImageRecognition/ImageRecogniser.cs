﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.OCR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using API.ImageRecognition;
using AutoMapper;
using CCSMapper.API.TextRecognition.DTO;
using CCSMapper.API.UconRecognition;

namespace ImageRecognition
{
    public class ImageRecogniser
    {
        private readonly ImageRecognitionOptions _options;

        public ImageRecogniser(ImageRecognitionOptions options)
        {
            this._options = options;
        }
        public RecognisedImageDetail Recognise(string filePath)
        {
            //Load the image from file and resize it for display
            Image<Bgr, Byte> img =
               new Image<Bgr, byte>(filePath)
               .Resize(400, 400, Emgu.CV.CvEnum.Inter.Linear, true);

            //Convert the image to grayscale and filter out the noise
            UMat uimage = new UMat();
            CvInvoke.CvtColor(img, uimage, ColorConversion.Bgr2Gray);

            //use image pyr to remove noise
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(uimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, uimage);

            Image<Gray, Byte> gray = img.Convert<Gray, Byte>().PyrDown().PyrUp();
            
            CircleF[] circles = CvInvoke.HoughCircles(gray, HoughType.Gradient, _options.InverseAccumulatorRatio,
                _options.MinDistBetweenCircles, _options.CannyThreshold, _options.CircleAccumulatorThreshold, 
                _options.MinRadius,_options.MaxRadius);

            Image<Bgr, Byte> circleImage = img.CopyBlank();
            foreach (CircleF circle in circles)
            {
                circleImage.Draw(circle, new Bgr(Color.Brown), 2);
            }

 


            string tessdata = Path.Combine(Directory.GetCurrentDirectory(), "tessdata");
            Tesseract tessEngine = new Tesseract(tessdata, "eng", OcrEngineMode.Default);
            // tessEngine.SetVariable("tessedit_char_whitelist", "0123456789.ABCD");

            tessEngine.SetImage(gray);
            tessEngine.Recognize();

            var tessCharacters = tessEngine.GetCharacters();
            
            var recognisedCharacters = (from c in tessCharacters
                                        select Mapper.Map<RecognisedCharacterDetail>(c));

            foreach(var characters in recognisedCharacters)
            {
                circleImage.Draw(characters.Text, characters.Region.Location, FontFace.HersheyPlain, 2, new Bgr(Color.Brown), 2);
            }


            var recognisedShapes = (from c in circles
                                    select Mapper.Map<UconShapeDetail>(c)).ToList();


            var detail = new RecognisedImageDetail()
            {
                RecognisedCharacters = recognisedCharacters,
                Image = img.ToBitmap(),
                RecognisedShapes = recognisedShapes,
                CircleMask = circleImage.ToBitmap()
            };
            return detail;
        }
    }
}
