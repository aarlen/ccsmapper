navigator.getUserMedia = ( navigator.getUserMedia ||
navigator.webkitGetUserMedia ||
navigator.mozGetUserMedia ||
navigator.msGetUserMedia);

var video;
var webcamStream;
var canvas, ctx;
let imageProcessResults;
function startWebcam() {
    if (navigator.getUserMedia) {
        navigator.getUserMedia (
            {
                video: true,
                audio: false
            },
            function(localMediaStream) {
                video.src = window.URL.createObjectURL(localMediaStream);
                webcamStream = localMediaStream;
            },
            function(err) {
                console.log("The following error occured: " + err);
            }
        );
    } else {
        console.log("getUserMedia not supported");
    }  
}

function snapshot() {
    ctx.drawImage(video, 0,0, canvas.width, canvas.height);
    const image = canvas.toDataURL("image/png");
    const encodedImage = encodeURI(image);
    imageProcessResults[0].value = "got image, sending to server";
    $.ajax({
        url : '/processImage',
        type : "POST",
        data : {
             image : encodedImage
        },
        dataType: "text",
        success : function(result) {
            console.log(result);
            imageProcessResults[0].value = result;
        }
    });
}

function stopWebcam() {
    console.log("stopWebCam()");
    video.pause();
}

function init() {
    video = document.querySelector('video');
    canvas = document.getElementById("myCanvas");
    ctx = canvas.getContext('2d');
    imageProcessResults = $("#imageProcessResults");
    
    $("#startWebcam").on("click",startWebcam);
    $("#stopWebcam").on("click",stopWebcam);
    $("#snapshot").on("click",snapshot);
}

$(document).ready(() => {
    init();
    console.log("ready");
});