const express = require('express');
const path = require('path');
const services = require('./services');
var server = express();

var bodyParser = require('body-parser')
server.use( bodyParser.json() );       // to support JSON-encoded bodies
server.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true,
  limit : "50mb"
})); 

server.use(express.static(__dirname));
services.initialize(server);
server.listen(3000, () => console.log('listening on port 3000!'));