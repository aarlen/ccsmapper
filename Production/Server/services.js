const fs = require('fs');
const exec = require('child_process').exec;

exports.initialize = function(server) {
    const registerUri = function(uri,response) {
        server.get(uri, response);
        server.post(uri, response);
        server.head(uri, response); 
    }

    function executeMapper(result) {
        console.log("executing mapper");
        exec(`runCCSMapper.bat`,(error,stdout,stderr) => {
            result(stdout);
        });
    }

    function processLastImage(req, res, next) {
        executeMapper((stdout) => {
            console.log(stdout);
            res.send(stdout);
            next();
        });
    }

    function processImage(req, res, next) {
        const image = req.body.image;
        const decodedImage = decodeURI(image);
        const data = decodedImage.replace(/^data:image\/\w+;base64,/, "");
        const buf = new Buffer(data, 'base64');

        const fileName = __dirname + "\\..\\..\\Run\\CapturedImage.png";
        console.log(`writing to ${fileName}`);
        fs.writeFile(fileName, buf, function(err) {
            if(err) {
                return console.log(err);
            }
        
            console.log("The file was saved!");

            executeMapper((stdout) => {
                console.log(stdout);
                res.end(stdout);
            });

            
        }); 
    }
    
    registerUri('/processImage', processImage);
    registerUri('/processLastImage', processLastImage);
    
    console.log('registered all services');
}