﻿using CCSMapper.API.TextRecognition;
using CCSMapper.API.TextRecognition.DTO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CCSMapper.TextRecognition
{
    public class TextRecognition : ITextRecognition
    {
        private int _verticalTolerance;
        public TextRecognition(int verticalTolerance)
        {
            _verticalTolerance = verticalTolerance;
        }

        private bool SameHeight(RecognisedCharacterDetail left,RecognisedCharacterDetail right)
        {
            return Math.Abs(left.Region.Top - right.Region.Top) <= _verticalTolerance;
        }

        private bool IsAfter(RecognisedCharacterDetail left, RecognisedCharacterDetail right)
         {
             return left.Region.Left <= right.Region.Left;
         }

        private IEnumerable<RecognisedCharacterDetail> FindNextWord(IEnumerable<RecognisedCharacterDetail> details,out IEnumerable<RecognisedCharacterDetail> word)
        {
            var result = new LinkedList<RecognisedCharacterDetail>();
            var first = details.First();
            var rest = details.Skip(1);

            var node = result.AddFirst(first);

            foreach(var other in rest)
            {
                if(SameHeight(node.Value,other))
                {
                    var before = node;
                    if (before.Next != null)
                    {
                        before = before.Next;
                        while (before.Next != null && IsAfter(before.Value, other))
                        {
                            before = before.Next;
                        }
                    }
                    result.AddAfter(before, other);

                }
            }

            word = result;

            return details.Except(result);
        }

        public IEnumerable<RecognisedWordDetail> ComputeStrings(IEnumerable<RecognisedCharacterDetail> details)
        {
            details = from d in details
                      where Char.IsLetterOrDigit(d.Text, 0)
                      orderby d.Region.Left
                      select d;
        
            var results = new List<RecognisedWordDetail>();

            while (details.Count() > 0)
            {
                IEnumerable<RecognisedCharacterDetail> word;
                details = FindNextWord(details, out word);

                int l = Int32.MaxValue;
                int t = Int32.MaxValue;
                int r = 0;
                int b = 0;
                StringBuilder text = new StringBuilder();

                foreach(var wor in word)
                {
                    text.Append(wor.Text);
                    if (l > wor.Region.Left) l = wor.Region.Left;
                    if (t > wor.Region.Top) t = wor.Region.Top;
                    if (r < wor.Region.Right) r = wor.Region.Right;
                    if (b < wor.Region.Bottom) b = wor.Region.Bottom;
                }


                results.Add(new RecognisedWordDetail()
                {
                    Text = text.ToString(),
                    Region = new Rectangle()
                    {
                        X = l,
                        Y = t,
                        Width = r - l,
                        Height = b - t
                    }
                });
            }
            return results;
        }
    }
}
