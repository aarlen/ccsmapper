﻿using CCSMapper.API.TextRecognition.DTO;
using CCSMapper.API.UconRecognition;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace CCSMapper.UconRecognition
{
    public class UconRecognizer
    {
        private bool Near(UconShapeDetail shape, RecognisedWordDetail word)
        {
            float tolerance = 200;
            PointF mid = new PointF(word.Region.Left + (float)((word.Region.Right - word.Region.Left) * 0.5),
                word.Region.Top + (float)((word.Region.Bottom - word.Region.Top) * 0.5));


            if(Math.Abs(shape.Center.X - mid.X) <= tolerance &&
                (mid.Y - shape.Center.Y)  <= tolerance)
            {
                return true;
            }

            return false;
        }

        public IEnumerable<UconDetail> Search(IEnumerable<UconShapeDetail> shapes,IEnumerable<RecognisedWordDetail> recognisedWords)
        {
            var results = new List<UconDetail>();

            foreach(var shape in shapes)
            {
                var overlapingTextShape = from w in recognisedWords
                                      where Near(shape, w)
                                      select w;

                if (overlapingTextShape.Count() > 0)
                {

                    int left = int.MaxValue, top = int.MaxValue, right = 0, bottom = 0;
                    foreach (var text in overlapingTextShape)
                    {
                        if (left > text.Region.Left) left = text.Region.Left;
                        if (top > text.Region.Top) top = text.Region.Top;
                        if (right < text.Region.Right) right = text.Region.Right;
                        if (bottom < text.Region.Bottom) bottom = text.Region.Bottom;
                    }

                    var uconDetail = new UconDetail()
                    {
                        Text = overlapingTextShape.Select(o => o.Text),
                        Region = new Rectangle(left, top, right - left, bottom - top)
                    };

                    results.Add(uconDetail);
                }

            }

            return results;
        }
    }
}
