﻿using System;
using CCSMapper.TextRecognition;
using CCSMapper.API.TextRecognition.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using System.Linq;
using System.Drawing;

namespace CCSMapperUnitTests
{
    [TestClass]
    public class TextRecognitionTests
    {
        private const int VerticalTolerance = 10;
        private TextRecognition textRecognition;
        [TestInitialize]
        public void Setup()
        {
            textRecognition = new TextRecognition(VerticalTolerance);
        }

        [TestMethod]
        public void ComputeStrings_With1Char_ResultsValidString()
        {
            var details = new RecognisedCharacterDetail[]
            {
                new RecognisedCharacterDetail()
                {
                    Text = "A",
                    Region = new Rectangle()
                    {
                        X = 0,
                        Y = 0,
                        Width = 50,
                        Height = 50
                    }
                }
            };

            var results = textRecognition.ComputeStrings(details.ToArray());

            results.FirstOrDefault().Text.Should().Be("A");
            results.FirstOrDefault().Region.Should().BeEquivalentTo<Rectangle>(new Rectangle()
            {
                X = 0,
                Y = 0,
                Width = 50,
                Height = 50
            });
        }

        [TestMethod]
        public void ComputeStrings_WithSampleDataInSequence_ResultsValidString()
        {
            var details = new RecognisedCharacterDetail[]
            {
                new RecognisedCharacterDetail()
                {
                    Text = "A",
                    Region = new Rectangle()
                    {
                        X = 0,
                        Y = 0,
                        Width = 50,
                        Height = 50
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "B",
                    Region = new Rectangle()
                    {
                        X = 50,
                        Y = 10,
                        Width = 50,
                        Height = 50
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "C",
                    Region = new Rectangle()
                    {
                        X = 100,
                        Y = 5,
                        Width = 50,
                        Height = 50
                    }
                }
            };

            var results = textRecognition.ComputeStrings(details.ToArray());

            results.FirstOrDefault().Text.Should().Be("ABC");
            results.FirstOrDefault().Region.Should().BeEquivalentTo<Rectangle>(new Rectangle()
            {
                X = 0,
                Y = 0,
                Width = 150,
                Height = 60
            });
        }

        [TestMethod]
        public void ComputeStrings_WithSampleDataOutSequence_ResultsValidString()
        {
            var details = new RecognisedCharacterDetail[]
            {
                new RecognisedCharacterDetail()
                {
                    Text = "B",
                    Region = new Rectangle()
                    {
                        X = 50,
                        Y = 10,
                        Width = 50,
                        Height = 50
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "A",
                    Region = new Rectangle()
                    {
                        X = 0,
                        Y = 0,
                        Width = 50,
                        Height = 50
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "C",
                    Region = new Rectangle()
                    {
                        X = 150,
                        Y = 5,
                        Width = 50,
                        Height = 50
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "D",
                    Region = new Rectangle()
                    {
                        X = 100,
                        Y = 5,
                        Width = 50,
                        Height = 50
                    }
                }
            };

            var results = textRecognition.ComputeStrings(details.ToArray());

            results.FirstOrDefault().Text.Should().Be("ABDC");
            results.FirstOrDefault().Region.Should().BeEquivalentTo<Rectangle>(new Rectangle()
            {
                X = 0,
                Y = 0,
                Width = 200,
                Height = 60
            });
        }

        [TestMethod]
        public void ComputeStrings_WithSeparateChars_ResultsValidString()
        {
            var details = new RecognisedCharacterDetail[]
            {
                new RecognisedCharacterDetail()
                {
                    Text = "A",
                    Region = new Rectangle()
                    {
                        X = 0,
                        Y = 0,
                        Width = 50,
                        Height = 50
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "B",
                    Region = new Rectangle()
                    {
                        X = 50,
                        Y = 50,
                        Width = 50,
                        Height = 50
                    }
                }
            };

            var results = textRecognition.ComputeStrings(details.ToArray());

            results.FirstOrDefault().Text.Should().Be("A");
            results.ElementAt(1).Text.Should().Be("B");
        }

        [TestMethod]
        public void ComputeStrings_WithSeparateCharsInTheMiddle_ResultsValidString()
        {
            var details = new RecognisedCharacterDetail[]
            {
                new RecognisedCharacterDetail()
                {
                    Text = "C",
                    Region = new Rectangle()
                    {
                        X = 160,
                        Y = 200,
                        Width = 15,
                        Height = 25
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "A",
                    Region = new Rectangle()
                    {
                        X = 180,
                        Y = 200,
                        Width = 15,
                        Height = 25
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "M",
                    Region = new Rectangle()
                    {
                        X = 200,
                        Y = 200,
                        Width = 15,
                        Height = 25
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = " ",
                    Region = new Rectangle()
                    {
                        X = 0,
                        Y = 0,
                        Width = 0,
                        Height = 0
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "1",
                    Region = new Rectangle()
                    {
                        X = 240,
                        Y = 200,
                        Width = 15,
                        Height = 25
                    }
                }
            };

            var results = textRecognition.ComputeStrings(details.ToArray());

            results.FirstOrDefault().Text.Should().Be("CAM1");
        }

        [DataTestMethod]
        [DataRow("'")]
        [DataRow(" ")]
        [DataRow("/")]
        public void ComputeStrings_WithInvalidChar_ResultsValidString(string invalid)
        {
            var details = new RecognisedCharacterDetail[]
            {
                new RecognisedCharacterDetail()
                {
                    Text = invalid,
                    Region = new Rectangle()
                    {
                        X = 0,
                        Y = 0,
                        Width = 50,
                        Height = 50
                    }
                },
                new RecognisedCharacterDetail()
                {
                    Text = "B",
                    Region = new Rectangle()
                    {
                        X = 50,
                        Y = 50,
                        Width = 50,
                        Height = 50
                    }
                }
            };

            var results = textRecognition.ComputeStrings(details.ToArray());

            results.FirstOrDefault().Text.Should().Be("B");
            results.FirstOrDefault().Region.Should().BeEquivalentTo<Rectangle>(new Rectangle()
            {
                X = 50,
                Y = 50,
                Width = 50,
                Height = 50
            });
        }
    }
}
