﻿using System;
using CCSMapper.UconRecognition;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using CCSMapper.API.UconRecognition;
using System.Collections.Generic;
using CCSMapper.API.TextRecognition.DTO;
using System.Drawing;
using System.Linq;

namespace UconRecognitionUnitTests
{
    [TestClass]
    public class UconRecognizerTests
    {
        private UconRecognizer _uconRecognizer = new UconRecognizer();

        [TestMethod]
        public void Search_WithoutData_ResultsNoUcons()
        {
            var results = _uconRecognizer.Search(new List<UconShapeDetail>(),new List<RecognisedWordDetail>());

            results.Should().BeEmpty();
        }

        [TestMethod]
        public void Search_With1ShapeButInvalid_Results0Ucon()
        {
            var shapes = new List<UconShapeDetail>()
            {
                new UconShapeDetail()
                {
                    Center = new PointF(0,0),
                    Radius = 1
                }
            };

            var words = new List<RecognisedWordDetail>()
            {
                new RecognisedWordDetail()
                {
                    Text = "AB",
                    Region = new Rectangle(200,50,100,100)
                }
            };


            var results = _uconRecognizer.Search(shapes, words);

            results.Should().BeEmpty();

        }

        [TestMethod]
        public void Search_With1Shape_Results1Ucon()
        {
            var shapes = new List<UconShapeDetail>()
            {
                new UconShapeDetail()
                {
                    Center = new PointF(0,0),
                    Radius = 1
                }
            };

            var words = new List<RecognisedWordDetail>()
            {
                new RecognisedWordDetail()
                {
                    Text = "AB",
                    Region = new Rectangle(0,0,100,100)
                },
                new RecognisedWordDetail()
                {
                    Text = "CD",
                    Region = new Rectangle(0,50,100,100)
                }
            };

            
            var results = _uconRecognizer.Search(shapes,words);

            results.Should().HaveCount(1);
            results.First().Text.Should().HaveCount(2);
            results.FirstOrDefault().Text.ElementAt(0).Should().Be("AB");
            results.FirstOrDefault().Text.ElementAt(1).Should().Be("CD");
            results.FirstOrDefault().Region.Should().BeEquivalentTo<Rectangle>(new Rectangle()
            {
                X = 0,
                Y = 0,
                Width = 100,
                Height = 150
            });

        }

        [TestMethod]
        public void Search_With2ShapeBut1InvalidUcon_Results1Ucon()
        {
            var shapes = new List<UconShapeDetail>()
            {
                new UconShapeDetail()
                {
                    Center = new PointF(0,0),
                    Radius = 1
                },
                new UconShapeDetail()
                {
                    Center = new PointF(1000,1000),
                    Radius = 1
                }
            };

            var words = new List<RecognisedWordDetail>()
            {
                new RecognisedWordDetail()
                {
                    Text = "AB",
                    Region = new Rectangle(0,0,100,100)
                },
                new RecognisedWordDetail()
                {
                    Text = "CD",
                    Region = new Rectangle(0,50,100,100)
                }
            };


            var results = _uconRecognizer.Search(shapes, words);

            results.Should().HaveCount(1);
            results.FirstOrDefault().Text.ElementAt(0).Should().Be("AB");
            results.FirstOrDefault().Text.ElementAt(1).Should().Be("CD");
            results.FirstOrDefault().Region.Should().BeEquivalentTo<Rectangle>(new Rectangle()
            {
                X = 0,
                Y = 0,
                Width = 100,
                Height = 150
            });

        }
    }
}
